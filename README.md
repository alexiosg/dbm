# dbm #

The dbm package implements the dynamic binary model of Kauppi and Saikkonen (2008), with optional L2 regularization and methods for estimation, forecasting (single and multi-step) and inference. It is still in beta and not fully tested, so feel free to provide any bug reports or contributions.