#include <R.h>
#include <Rinternals.h>
#include <stdlib.h> // for NULL
#include <R_ext/Rdynload.h>

/* FIXME: 
 Check these declarations against the C/Fortran source code.
 */

/* .C calls */
extern void c_dbmderiv1(void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *);
extern void c_dbmderiv2(void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *);
extern void c_dbmderiv3(void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *);
extern void c_dbmestimate(void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *);
extern void c_dbmfilter(void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *);

/* .Call calls */
extern SEXP dbmpk1(SEXP, SEXP, SEXP, SEXP, SEXP, SEXP, SEXP, SEXP, SEXP, SEXP, SEXP);
extern SEXP dbmpk2(SEXP, SEXP, SEXP, SEXP, SEXP, SEXP, SEXP, SEXP, SEXP, SEXP, SEXP);

static const R_CMethodDef CEntries[] = {
    {"c_dbmderiv1",   (DL_FUNC) &c_dbmderiv1,   25},
    {"c_dbmderiv2",   (DL_FUNC) &c_dbmderiv2,   25},
    {"c_dbmderiv3",   (DL_FUNC) &c_dbmderiv3,   29},
    {"c_dbmestimate", (DL_FUNC) &c_dbmestimate, 15},
    {"c_dbmfilter",   (DL_FUNC) &c_dbmfilter,   12},
    {NULL, NULL, 0}
};

static const R_CallMethodDef CallEntries[] = {
    {"dbmpk1", (DL_FUNC) &dbmpk1, 11},
    {"dbmpk2", (DL_FUNC) &dbmpk2, 11},
    {NULL, NULL, 0}
};

void R_init_dbm(DllInfo *dll)
{
    R_registerRoutines(dll, CEntries, CallEntries, NULL, NULL);
    R_useDynamicSymbols(dll, FALSE);
}
